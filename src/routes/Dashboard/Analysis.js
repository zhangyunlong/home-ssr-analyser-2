import React, { Component } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col,
} from 'antd';
import ssrImage from '../../assets/dashboard.png';
import timeImage from '../../assets/timer.gif';
import manual from '../../assets/GeneMarker_UserManual.pdf';
import ssrAnalyser from '../../assets/SSR_Analyser_V1.2.4_2015082120151120.rar';
import ssrStarter from '../../assets/ssr-stater.exe';
// import styles from './Analysis.less';

const { Meta } = Card;

@connect(({ chart, loading }) => ({
  chart,
  loading: loading.effects['chart/fetch'],
}))
export default class Analysis extends Component {
  render() {
    return (
      <div>
        <Row gutter={24}>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Card
              title="SSR指纹分析器"
              extra={<a href={ssrAnalyser}>下载</a>}
              cover={<img alt="example" src={ssrImage} />}
              style={{ marginLeft: '20px' }}
            >
              <Meta
                title="MD5:"
                description="5091dd3a0203055917d91c179b3180d0"
              />
            </Card>
            <Card
              title="用户使用手册"
              extra={<a href={manual} download="GeneMarker UserMenual.pdf">下载</a>}
              style={{ marginLeft: '20px', marginTop: '15px' }}
            >
              <Meta
                description="GeneMarker UserManual"
              />
            </Card>
          </Col>
          <Col xl={12} lg={24} md={24} sm={24} xs={24}>
            <Card
              title="试用版启动器"
              extra={<a href={ssrStarter}>下载</a>}
              cover={<img alt="example" src={timeImage} />}
              style={{ marginRight: '20px' }}
            >
              <Meta
                title="MD5:"
                description="ec8le4b8388b57674a0b57fa7ae0a9lf"
              />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
